# Heart disease (Neural Network)

### By: Andrew Wairegi

## Description
To predict whether a patient will have heart disease (CHD/MI) or not, using a machine learning model.
This should predict whether they have CHD/MI, with an accuracy of atleast 80%. We also want to look
at any of the columns to see if they are a good predictor of these diseases.

[Open notebook]

## Setup/installation instructions
1. Find a local folder on your computer
2. Set it up as an empty repository (git init)
3. Clone this repository there (git clone https://...)
4. Upload the notebook to google drive
5. Open it
6. Upload the data file to google collab (the file upload section)
7. Run the notebook

## Known Bugs
There are no known issues / bugs.

## Technologies Used
1. Python - The programming language
2. Numpy - An Arithmetic Package
3. Pandas - A Data Analysis Package
4. Seaborn - A Visualization package
5. Matplotlib - A Visualization package
6. Scikit learn - A Modelling package

<br>

### License
Copyright © 2021 **Andrew Wairegi**
<br>
You are free to view, but not copy.

[Open notebook]: https://gitlab.com/d7713/machine-learning/NeuralNetwork-Heart_disease_classification/-/blob/main/Heart%20disease%20(neural%20network).ipynb
